//
//  Response.swift
//  APIMarvelChallenge
//
//  Created by Andre Paganin on 27/05/19.
//  Copyright © 2019 Andre Paganin. All rights reserved.
//

import Foundation

struct Response<T>: Decodable where T: Decodable {
    
    var code: Int?
    
    var status: String?
    
    var copyright: String?
    
    var attributionText: String?
    
    var attributionHTML: String?
    
    var eTag: String?
    
    var data: ResponseData<T>?
    
    private enum CodingKeys: String, CodingKey {
        case code
        case status
        case copyright
        case attributionText
        case attributionHTML
        case eTag = "etag"
        case data
    }
}


