//
//  Events.swift
//  APIMarvelChallenge
//
//  Created by Andre Paganin on 27/05/19.
//  Copyright © 2019 Andre Paganin. All rights reserved.
//

import Foundation

struct Events: Resource, Decodable {
    
    var resourceURI: URL?
    
    var name: String?
}
