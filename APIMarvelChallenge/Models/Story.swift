//
//  Story.swift
//  APIMarvelChallenge
//
//  Created by Andre Paganin on 27/05/19.
//  Copyright © 2019 Andre Paganin. All rights reserved.
//

import Foundation

struct Story: Resource, Decodable {
    
    var resourceURI: URL?
    
    var name: String?
    
    var type: StoryType = .unknown
}

