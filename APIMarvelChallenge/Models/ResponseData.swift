//
//  ResponseData.swift
//  APIMarvelChallenge
//
//  Created by Andre Paganin on 27/05/19.
//  Copyright © 2019 Andre Paganin. All rights reserved.
//

import Foundation

struct ResponseData<T>: Decodable where T: Decodable {
    
    var offset: Int?
    
    var limit: Int?
    
    var total: Int?
    
    var count: Int?
    
    var results: T?
}


