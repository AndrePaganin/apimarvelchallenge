//
//  CollectionInfo.swift
//  APIMarvelChallenge
//
//  Created by Andre Paganin on 27/05/19.
//  Copyright © 2019 Andre Paganin. All rights reserved.
//

import Foundation

struct CollectionInfo<T>: Decodable where T: Resource, T: Decodable {
    
    var available: Int?
    
    var collectionURI: URL?
    
    var returned: Int?
    
    var items: [T]?
}

