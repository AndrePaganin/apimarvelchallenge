//
//  Thumbnail.swift
//  APIMarvelChallenge
//
//  Created by Andre Paganin on 27/05/19.
//  Copyright © 2019 Andre Paganin. All rights reserved.
//

import Foundation

struct Thumbnail: Decodable {
    
    private var path: String?
    
    private var `extension`: String?
    
    var thumbURL: URL? {
        guard let path = self.path,
            let extensn = self.extension else { return nil }
        
        let stringURL = path + "." + extensn
        return URL(string: stringURL)
    }
}

