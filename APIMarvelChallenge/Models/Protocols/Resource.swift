//
//  Resource.swift
//  APIMarvelChallenge
//
//  Created by Andre Paganin on 27/05/19.
//  Copyright © 2019 Andre Paganin. All rights reserved.
//

import Foundation

protocol Resource {
    
    var resourceURI: URL? { get set }
    
    var name: String? { get set }
}

