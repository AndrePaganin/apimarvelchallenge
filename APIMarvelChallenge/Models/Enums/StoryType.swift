//
//  StoryType.swift
//  APIMarvelChallenge
//
//  Created by Andre Paganin on 27/05/19.
//  Copyright © 2019 Andre Paganin. All rights reserved.
//

enum StoryType: String, Decodable {
    
    case unknown
    case cover
    case interiorStory
    
    init(from decoder: Decoder) throws {
        
        do {
            let container = try decoder.singleValueContainer()
            let value = try container.decode(String.self)
            self = StoryType.init(rawValue: value) ?? .unknown
        }
        catch {
            self = .unknown
        }
    }
}



