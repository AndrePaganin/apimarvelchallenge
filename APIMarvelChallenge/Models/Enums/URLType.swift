//
//  URLType.swift
//  APIMarvelChallenge
//
//  Created by Andre Paganin on 27/05/19.
//  Copyright © 2019 Andre Paganin. All rights reserved.
//

enum URLType: String, Decodable {
    
    case unknown
    case detail
    case wiki
    case comiclink
    
    init(from decoder: Decoder) throws {
        
        do {
            let container = try decoder.singleValueContainer()
            let value = try container.decode(String.self)
            self = URLType.init(rawValue: value) ?? .unknown
        }
        catch {
            self = .unknown
        }
    }
}

