//
//  UIViewController+Extension.swift
//  APIMarvelChallenge
//
//  Created by Andre Paganin on 27/05/19.
//  Copyright © 2019 Andre Paganin. All rights reserved.
//

import UIKit

extension UIViewController {
    
    var isForceTouchEnabled: Bool {
        return traitCollection.forceTouchCapability == .available
    }
}


