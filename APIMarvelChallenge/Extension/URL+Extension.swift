//
//  URL+Extension.swift
//  APIMarvelChallenge
//
//  Created by Andre Paganin on 27/05/19.
//  Copyright © 2019 Andre Paganin. All rights reserved.
//

import Foundation

extension URL {
    
    init?(string: String?) {
        guard let string = string else { return nil }
        self.init(string: string)
    }
}
