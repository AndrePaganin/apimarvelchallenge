//
//  CharactersListVC+PreviewingDelegate.swift
//  APIMarvelChallenge
//
//  Created by Andre Paganin on 27/05/19.
//  Copyright © 2019 Andre Paganin. All rights reserved.
//

import UIKit

extension CharactersListVC: UIViewControllerPreviewingDelegate {
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing,
                           commit viewControllerToCommit: UIViewController) {
        
        navigationController?.pushViewController(viewControllerToCommit, animated: true)
    }
    
    func previewingContext(_ previewingContext: UIViewControllerPreviewing,
                           viewControllerForLocation location: CGPoint) -> UIViewController? {
        
        guard let indexPath = tableView.indexPathForRow(at: location) else { return nil }
        
        guard let detailsVC = Navigation.getViewController(type: CharactersDetailsVC.self,
                                                           identifer: "CharactersDetails") else { return nil }
        detailsVC.character = charactersArray[indexPath.row]
        
        return detailsVC
    }
}

