//
//  Configuration.swift
//  APIMarvelChallenge
//
//  Created by Andre Paganin on 27/05/19.
//  Copyright © 2019 Andre Paganin. All rights reserved.
//

struct Configuration {
    
    static let url = "http://gateway.marvel.com"
    
    static let apiKey = "ff3d4847092294acc724123682af904b"
    
    static let hash = "412b0d63f1d175474216fadfcc4e4fed"
    
    static func checkConfiguration() {
        
        if url.isEmpty || apiKey.isEmpty || hash.isEmpty {
            fatalError("""
                Erro ao carregar os dados da MARVEL. Verifique e tente novamente!
            """)
        }
    }
}

