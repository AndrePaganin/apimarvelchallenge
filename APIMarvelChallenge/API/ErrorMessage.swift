//
//  ErrorMessage.swift
//  APIMarvelChallenge
//
//  Created by Andre Paganin on 27/05/19.
//  Copyright © 2019 Andre Paganin. All rights reserved.
//

struct ErrorMessage {
    
    static let unknownErrorMsg = "Erro desconhecido. Verifique e tente novamente!"
    
    static let dataParsingErrorMsg = "Dados invalidos. Verifique e tente novamente!"
}

