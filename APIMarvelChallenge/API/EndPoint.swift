//
//  EndPoint.swift
//  APIMarvelChallenge
//
//  Created by Andre Paganin on 27/05/19.
//  Copyright © 2019 Andre Paganin. All rights reserved.
//

enum EndPoint: String {
    
    case publicCharacters   = "/v1/public/characters"
}

